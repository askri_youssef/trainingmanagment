package com.youssef.trainingmanagment.controller;

import java.util.List;

import javax.security.sasl.AuthenticationException;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.youssef.trainingmanagment.exception.ResourceNotFoundException;
import com.youssef.trainingmanagment.model.AuthClass;
import com.youssef.trainingmanagment.model.Student;
import com.youssef.trainingmanagment.repository.StudentRepository;

/**
 * @author Youssef ASKRI
 *
 */

@RestController
@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600, methods = { RequestMethod.GET, RequestMethod.POST,
		RequestMethod.PUT, RequestMethod.DELETE })
@RequestMapping("/student")
public class StudentController {

	@Autowired
	StudentRepository studentRepository;

	// Get list Students
	@GetMapping("/")
	public List<Student> getAllStudent() {
		return studentRepository.findAll();
	}

	// Get student by Login
	@GetMapping("/{login}")
	public Student getStudent(@PathVariable(value = "login") String login) {

		Student student = studentRepository.findByLogin(login);
		if (student == null) {
			throw new ResourceNotFoundException("Student", "login", login);
		}
		return student;
	}

	// Create Student
	@PostMapping("/")
	public Student createStudent(@Valid @RequestBody Student student) {
		return studentRepository.save(student);
	}

	// Update Student
	@PutMapping("/{login}")
	public Student updateStudent(@PathVariable(value = "login") String login,
			@Valid @RequestBody Student studentDetails) {

		Student student = studentRepository.findByLogin(login);
		if (student == null) {
			throw new ResourceNotFoundException("Student", "login", login);
		}
		studentDetails.setLogin(login);
		return studentRepository.save(studentDetails);
	}

	// Delete Student
	@DeleteMapping("/{login}")
	public ResponseEntity<?> deleteStudent(@PathVariable(value = "login") String login) {
		Student student = studentRepository.findByLogin(login);
		if (student == null) {
			throw new ResourceNotFoundException("Student", "login", login);
		}

		studentRepository.delete(student);

		return ResponseEntity.ok().build();
	}

	@PostMapping("/auth/login")
	public Student getIfExistStudent(@RequestBody AuthClass authClass) throws AuthenticationException {
		Student student = studentRepository.findByLogin(authClass.getEmail());
		if (student == null) {
			throw new ResourceNotFoundException("Student", "login", authClass.getEmail());
		} else if (!student.getPwd().equals(authClass.getPassword())) {
			throw new AuthenticationException("Password is invalid for " + authClass.getEmail());
		}

		return student;

	}
}
