package com.youssef.trainingmanagment.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.youssef.trainingmanagment.model.Training;

/**
 * @author Youssef ASKRI
 *
 */
public interface TrainingRepository extends JpaRepository<Training, Long> {

}
