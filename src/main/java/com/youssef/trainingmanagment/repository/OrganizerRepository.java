package com.youssef.trainingmanagment.repository;

/**
 * @author Youssef ASKRI
 *
 */

import org.springframework.data.jpa.repository.JpaRepository;

import com.youssef.trainingmanagment.model.Organizer;

public interface OrganizerRepository extends JpaRepository<Organizer, Long> {

}
