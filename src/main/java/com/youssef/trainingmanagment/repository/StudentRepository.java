package com.youssef.trainingmanagment.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.youssef.trainingmanagment.model.Student;

/**
 * @author Youssef ASKRI
 *
 */

@Repository
public interface StudentRepository extends JpaRepository<Student, Long> {

	@Query("Select s from Student s where s.login like %:login%")
	Student findByLogin(@Param("login") String login);

}
