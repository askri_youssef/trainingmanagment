package com.youssef.trainingmanagment.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQuery;

/**
 * The persistent class for the student database table.
 * 
 */
@Entity
@NamedQuery(name = "Student.findAll", query = "SELECT s FROM Student s")
public class Student implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	// @GeneratedValue(generator = "uuid")
	// @GenericGenerator(name = "uuid", strategy = "uuid2")
	private String login;

	private String affiliation;

	private int age;

	private String email;

	private String firstName;

	private String name;

	private String pwd;

	// bi-directional many-to-many association to Training
	@ManyToMany(mappedBy = "students", fetch = FetchType.EAGER)
	private List<Training> trainings;

	public Student() {
	}

	public Student(String login, String password, String nom, String prenom, String email2, int age2,
			String affiliation2) {
		// TODO Auto-generated constructor stub
		this.login = login;
		this.pwd = password;
		this.name = nom;
		this.firstName = prenom;
		this.email = email2;
		this.age = age2;
		this.affiliation = affiliation2;
	}

	public Student(String login) {
		super();
		this.login = login;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getAffiliation() {
		return this.affiliation;
	}

	public void setAffiliation(String affiliation) {
		this.affiliation = affiliation;
	}

	public int getAge() {
		return this.age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPwd() {
		return this.pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	public List<Training> getTrainings() {
		return this.trainings;
	}

	public void setTrainings(List<Training> trainings) {
		this.trainings = trainings;
	}

	@Override
	public String toString() {
		return "Student [login=" + login + ", affiliation=" + affiliation + ", age=" + age + ", email=" + email
				+ ", firstName=" + firstName + ", name=" + name + ", pwd=" + pwd + ", trainings=" + trainings + "]";
	}

}