package com.youssef.trainingmanagment.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the training database table.
 * 
 */
@Entity
@NamedQuery(name="Training.findAll", query="SELECT t FROM Training t")
public class Training implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int trainingId;

	private int hoursNbr;

	private String prerequisites;

	private String subject;

	private String targetPopu;

	//bi-directional many-to-one association to Organizer
	@ManyToOne
	@JoinColumn(name="organizerId")
	private Organizer organizer;

	//bi-directional many-to-many association to Student
	@ManyToMany(fetch=FetchType.EAGER)
	@JoinTable(
		name="subscription"
		, joinColumns={
			@JoinColumn(name="trainingId")
			}
		, inverseJoinColumns={
			@JoinColumn(name="studentLogin")
			}
		)
	private List<Student> students;

	public Training() {
	}

	public Training(int id, String subject) {
		// TODO Auto-generated constructor stub
		this.trainingId=id;
		this.subject=subject;
	}


	public Training(int trainingId, int hoursNbr, String prerequisites, String subject, String targetPopu) {
		super();
		this.trainingId = trainingId;
		this.hoursNbr = hoursNbr;
		this.prerequisites = prerequisites;
		this.subject = subject;
		this.targetPopu = targetPopu;
	}
	
	

	public Training(int trainingId, int hoursNbr, String prerequisites, String subject, String targetPopu,
			List<Student> students) {
		super();
		this.trainingId = trainingId;
		this.hoursNbr = hoursNbr;
		this.prerequisites = prerequisites;
		this.subject = subject;
		this.targetPopu = targetPopu;
		this.students = students;
	}

	public int getTrainingId() {
		return this.trainingId;
	}

	public void setTrainingId(int trainingId) {
		this.trainingId = trainingId;
	}

	public int getHoursNbr() {
		return this.hoursNbr;
	}

	public void setHoursNbr(int hoursNbr) {
		this.hoursNbr = hoursNbr;
	}

	public String getPrerequisites() {
		return this.prerequisites;
	}

	public void setPrerequisites(String prerequisites) {
		this.prerequisites = prerequisites;
	}

	public String getSubject() {
		return this.subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getTargetPopu() {
		return this.targetPopu;
	}

	public void setTargetPopu(String targetPopu) {
		this.targetPopu = targetPopu;
	}

	public Organizer getOrganizer() {
		return this.organizer;
	}

	public void setOrganizer(Organizer organizer) {
		this.organizer = organizer;
	}

	public List<Student> getStudents() {
		return this.students;
	}

	public void setStudents(List<Student> students) {
		this.students = students;
	}

	@Override
	public String toString() {
		return "Training [trainingId=" + trainingId + ", hoursNbr=" + hoursNbr + ", prerequisites=" + prerequisites
				+ ", subject=" + subject + ", targetPopu=" + targetPopu + ", organizer=" + organizer + ", students="
				+ students + "]";
	}

}