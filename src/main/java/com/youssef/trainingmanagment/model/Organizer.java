package com.youssef.trainingmanagment.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the organizer database table.
 * 
 */
@Entity
@NamedQuery(name="Organizer.findAll", query="SELECT o FROM Organizer o")
public class Organizer implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int organizerId;

	private String address;

	private String name;

	private String tel;

	//bi-directional many-to-one association to Training
	@OneToMany(mappedBy="organizer", fetch=FetchType.EAGER)
	private List<Training> trainings;

	public Organizer() {
	}

	public int getOrganizerId() {
		return this.organizerId;
	}

	public void setOrganizerId(int organizerId) {
		this.organizerId = organizerId;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTel() {
		return this.tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public List<Training> getTrainings() {
		return this.trainings;
	}

	public void setTrainings(List<Training> trainings) {
		this.trainings = trainings;
	}

	public Training addTraining(Training training) {
		getTrainings().add(training);
		training.setOrganizer(this);

		return training;
	}

	public Training removeTraining(Training training) {
		getTrainings().remove(training);
		training.setOrganizer(null);

		return training;
	}

}